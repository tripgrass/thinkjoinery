<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>

<div class="block-generic_content module    " id="generic_content_block_1">
	<div class="layout-content">
		<div class=" interior-container interior-box ">
			<div class="gc-wrap ">
				<div class="gc-content">

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', 'search' ); ?>
		<?php endwhile; ?>

				</div>
			</div>
		</div>
	</div><!--/layout-content-->
</div>

<?php endif; ?>
<?php get_footer(); ?>