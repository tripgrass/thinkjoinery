<?php
/*
Template Name: Services
*/

$services = get_posts([
  'post_type' => 'service',
  'post_status' => 'publish',
  'numberposts' => -1
]);
 get_header(); ?>
<?php echo do_shortcode('[mason_build_blocks container=content]'); ?>

    <?php if ( have_posts() ) : ?>
                    <?php 
                    $html = "";
                    $links = "<div class='pilot-container'><div class='interior-box-small'><div class='service-links'>";
                    foreach($services as $wpService):
                        $service = new PilotService($wpService);
                       // print_r($service);
                        $description = get_field('service_description', $service->ID);
                        $links .= "<span style='display:inline-block' data-service='".$service->post_name."'><h6>".$service->post_name."</h6></span>";
                        $html .= "
                        <div class='block-headerbanner module'>
                          <div class='slides-section'>
                            <div class='feature-wrapper'>
                              <div class='slick-slider'>
                                  <img class='slides-block-image' src='".$service->thumbnail."'>
                              </div>
                            </div><!--/feature-wrapper-->
                          </div><!--/slides-section-->
                        </div>
                        <div class='pilot-container'>
                          <div class='service interior-box-wide' data-service='". $service->post_name."'>
                            <div class='content'>
                              <h1>".$service->post_title."</h1>
                              <div class='description'>".$description."</div>
                            </div><!--//content-->";
                          if(count($service->projects)> 0 ){
                          $html .= "<div class='sidebar-box shadow-card'>
                            <h6>RELATED PROJECTS</h6><p>";
                            foreach($service->projects as $project){
                              $html .= "<a href='".$project->permalink."'>".$project->post_title."</a>";
                            }
                          $html .="</p>
                        </div><!--/sidebar-box-->";
                          }
                          $html .= "<div style='clear:both;'></div></div><!--/service--></div><!--//pilot-container-->"
                    ?>
                    <?php endforeach; ?>
                    <?php 
                      $links .= "</div></div></div>";

                      echo $links;
                      echo $html; 
                    ?>
	<?php endif; ?>
  <?php echo do_shortcode('[mason_build_blocks container=lower]'); ?>
  <script>
    $( document ).ready(function() {
        $('.service-links span').click(function(){
          var target = $(this).data('service');
         // alert(target);
          $([document.documentElement, document.body]).animate({
        scrollTop: $(".service[data-service='" + target + "']").offset().top
    }, 1000);
        });
      });
  </script>
<?php get_footer(); ?>