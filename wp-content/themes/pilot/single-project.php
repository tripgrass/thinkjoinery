<?php get_header(); ?>
<?php while ( have_posts() ) : 
		the_post(); 
		$post = get_post();
		$project = new PilotProject($post);
		get_template_part( 'views/content', 'page' );
		$services = get_posts(array(
			'post_type' => 'service',
			'meta_query' => array(
				array(
					'key' => 'service_projects', // name of custom field
					'value' => '"' . get_the_ID() . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				)
			)
		));
		$html = "";
		$sidebar_items = get_field('sidebar');
		if(count($sidebar_items)> 0 ){
			$html .= "<div class='sidebar-box shadow-card'><p>";
			foreach($sidebar_items as $item){
			  $html .= "<div class='item'><h6>".$item['label']."</h6><p>".$item['value']."</p></div>";
			}
			$html .="</p></div><!--/sidebar-box-->";
		}
?>
<div class="block-generic_content module">
	<div class="layout-content">
		<div class=" interior-container interior-box-wide">
        	<div class='content'>
				<h1><?php echo $project->post_title; ?></h1>
                <div class='description'><?php echo get_field('description'); ?></div>
				<?php echo do_shortcode('[mason_build_blocks container=project]'); ?>
			</div>
            <?php echo $html; ?>
		</div>
		<div style="clear:both;"?></div>
	</div><!--/layout-content-->

<?php endwhile; ?>
	<div class="gallery">
		<?php 
			$images = get_field('gallery'); 
			if(is_array($images)):
				foreach($images as $image): ?>
					<div class="image"><img src="<?php echo $image['image']['url']; ?>"></div>
				<?php endforeach;
			endif;
		?>
	</div>
	<?php echo do_shortcode('[mason_build_blocks container=lower]'); ?>
<?php get_footer(); ?>