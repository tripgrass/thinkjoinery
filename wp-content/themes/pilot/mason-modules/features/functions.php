<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_features_layout(){
		$rows_arr = mason_get_sub_field('features_block_rows');
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$custom_link = $row_arr['features_block_button_custom_link'];		
				if( $custom_link ){ 
					$button_link = $custom_link;
				}
				else{
					$button_link = $row_arr['features_block_button_link'];
				}
				$rows[] = array(
					'title' => $row_arr['features_block_title'],
					'text' => $row_arr['features_block_text'],
					'image' => $row_arr['features_block_image'],
					'button_text' => $row_arr['features_block_button_text'],
					'button_link' => $button_link
				);
				
			}
			while ( mason_have_rows('features_block_rows') ) : the_row();
				$custom_link = get_sub_field('features_button_custom_link');		
				if( $custom_link ){ 
					$button_link = $custom_link;
				}
				else{
					$button_link = get_sub_field('features_block_button_link');
				}
				$rows[] = array(
					'title' => get_sub_field('features_block_title'),
					'text' => get_sub_field('features_block_text'),
					'image' => get_sub_field('features_block_image'),
					'button_text' => get_sub_field('features_block_button_text'),
					'button_link' => $button_link
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		return $args;
	}

?>