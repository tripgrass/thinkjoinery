<?php
	function build_navigation_layout(){
		$args = array(
			'logo' => mason_get_sub_field('navigation_block_logo'),
			'secondary_logo' => mason_get_sub_field('navigation_block_logo_secondary'),
			'main_links' => mason_get_sub_field('navigation_block_main_nav'),
			'social' => mason_get_sub_field('navigation_block_social_links'),
			'extra' => mason_get_sub_field('navigation_block_extra_links'),
			'directions' => mason_get_sub_field('navigation_block_directions')
		);

		return $args;
	}
?>