<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>
<div class="navigation" id="navigation_id">
	<div class="container-fluid container-lg">
		<div class="row">
			<div class="col-xs-8 col-md-4">
				<a href='/' class='home-anchor'>
				</a>
			</div>
			<div class="col-xs-4 col-md-8" id="mobile-nav-wrapper">
				<div class="mobile-nav">
					<div class="menu-button">
						<img class="menu-open menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-MENU-BLUE.svg">
						<img class="menu-up menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-UP-GRAY.svg">
					</div>
				</div>
			</div>			
			<div class="col-xs-4 col-md-8">
				<div class="main-nav">
					<?php $active_link_subs = ""; if( is_array($args['main_links']) && count($args['main_links'])>0): ?>
						<ul class="primary-nav">
							<?php foreach($args['main_links'] as $main): 
								$main_href = $main['navigation_block_main_link']['url'];
								$active_class = "";
								$active_link = 0;
								if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
									$active_class = " active";
									$active_link = 1;
								}
								$is_parent_class ="";
							?>
								<?php 
									$subs = "";
									if( is_array($main['navigation_block_sublinks']) && count($main['navigation_block_sublinks'])>0): 
										$is_parent_class = "menu-item-has-children";
								?>
									<?php foreach($main['navigation_block_sublinks'] as $sub ) : 
										if( isset($sub['navigation_block_sub_link']['url'])){
											$sub_href = $sub['navigation_block_sub_link']['url'];
											if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
												$active_class = " active";
												$active_link = 1;
											}
											$subs .= '<li><a href="'.$sub_href.'">'.$sub['navigation_block_sub_link']['title'].'</a></li>';
										}
									endforeach; ?>
								<?php 
									if( $active_link && $subs){
										$active_link_subs = $subs;
									}
									endif; 
								?>
								<li class="<?php echo $active_class. " ". $is_parent_class; ?>"><a href="<?php echo $main_href; ?>">
										<?php echo $main['navigation_block_main_link']['title']; ?>
									</a>
									<?php if($subs) : ?>
										<ul class="sub-nav">
											<?php echo $subs; ?>
										</ul>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="mobile-navigation">
	<div class="mobile-menu">
		<div class="menu-close-wrapper-close">
			<div class="menu-button-close">
				<img class="menu-close menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-CLOSE-GRAY.svg">						
			</div>
		</div>
		<div class="container-fluid container-lg container-md container-sm container-card">	
			<?php if( count($args['main_links'])>0): ?>
				<div class="primary-nav row">
					<?php foreach($args['main_links'] as $main): 
						$main_href = $main['navigation_block_main_link']['url'];
						$active_class = "";
						if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
							$active_class = " active";
						}
					?>
						<?php $subs = ""; if( is_array($main['navigation_block_sublinks']) && count($main['navigation_block_sublinks'])>0): ?>
							<?php foreach($main['navigation_block_sublinks'] as $sub ) : 
								if( isset($sub['navigation_block_sub_link']['url'])){
									$sub_href = $sub['navigation_block_sub_link']['url'];
									if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
										$active_class = " active";
									}
									$subs .= '<li><a href="'.$sub_href.'">'.$sub['navigation_block_sub_link']['title'].'</a></li>';
								}
							endforeach; ?>
						<?php endif; ?>
						<div class="menu-item-wrapper col-xs-12 col-sm-6 col-lg-3 <?php echo $active_class; ?>">
							<a href="<?php echo $main_href; ?>">
								<?php echo $main['navigation_block_main_link']['title']; ?>
							</a>
							<?php if($subs) : ?>
								<ul class="sub-nav">
									<?php echo $subs; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
<div class="mobile-and-footer row">
				<div class="mf-col menu-item-wrapper col-xs-12 col-sm-6 col-lg-3"><p class="small"><?php echo $args['directions']; ?></p></div>
				<div class="mf-col menu-item-wrapper col-xs-12 col-sm-6 col-lg-3">
					<p class="small">
						<?php if(is_array($args['extra'])) : ?>
							<?php foreach($args['extra'] as $arr) : $extra = $arr['navigation_block_extra_link']; ?>
								<?php if(is_array($extra)) : ?>
								<a href="<?php echo $extra['url']; ?>" target="<?php echo $extra['target']; ?>"><?php echo $extra['title']; ?></a> 
								<?php endif; ?>
							<?php endforeach; ?>	
						<?php endif; ?>
					</p>
				</div>
				<div class="mf-col menu-item-wrapper col-xs-12 col-sm-6 col-lg-3"><p class="small">
					<?php if(is_array($args['social'])) : ?>
						<?php foreach($args['social'] as $arr) : 
							$logo = $arr['navigation_block_social_logo']; 
							$link = $arr['navigation_block_social_link']; ?>
							<a class="social" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
								<img src="<?php echo $logo['url']; ?>">
							</a> 
						<?php endforeach; ?>				
					<?php endif; ?>
				</p></div>
			</div>
		</div>
	</div>
</div><!--/what-->