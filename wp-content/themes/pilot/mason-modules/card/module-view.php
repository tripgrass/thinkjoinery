<?php
	global $args;
		$block_class = "half-width";
		$isHeader = "";
		if($args['isHeader']) : 
			$isHeader = " isHeader";
		?>
		<script>
			$( document ).ready(function() {
				$('body').addClass('card-header');
			});
		</script>
	<?php endif; ?>

<div class="interior-container interior-box slower fadeIn animated <?php echo $isHeader; ?>" >
<?php
	if(count($args['cards']) > 0 ) :
		$i = 0;
		foreach($args['cards'] as $card):  ?>
			<?php if($i % 2 == 0 ) : ?>
				<div style="display:block">
			<?php endif; ?>
			<div class="shadow-card half-width">
				<div class="img-block-wrap" id="<?php echo $args['id']; ?>">
					<?php if($card['bg_image_url']) : ?>
						<div class="bg-image" style="background-image: url(' <?php echo $card['bg_image_url']; ?>');">
						</div><!--/bg-img-->
					<?php endif; ?>
				</div><!--img-block-wrap-->
				<div class="card-foot-content">
					<div class="blue-line"></div>
						<div class="title-wrap">
<?php $use_title = 0; ?>
							<?php if( $card['subtitle'] ) : ?>
								<h5><?php echo $card['subtitle']; ?></h5>
							<?php endif; ?>
							<?php if( $card['title']  ) : ?>
								<h2><?php echo $card['title']; ?></h2>
							<?php endif; ?>
							<?php if(  $card['content']  ) : ?>
								<?php echo $card['content']; ?>
							<?php endif; ?>
							<?php if( $card['button']  ) : ?>
								<div class="button-wrapper ">
									<a href="<?php echo $card['button']['url']; ?>" target="<?php echo $card['button']['target']; ?>" class="pilot-button">
										<h5><?php echo $card['button']['title']; ?></h5>
									</a>
								</div>
							<?php endif; ?>
						</div><!--/title-wrap-->
				</div><!--/card-foot-content-->
			</div><!--/shadow-card-->
			<?php if($i % 2 != 0 || ($i+1) == count($args['cards'])) : ?>
				 <br style="clear: both">
				</div><!--/row-->
			<?php endif; ?>
		<?php $i++; endforeach; ?>
	<?php endif; ?>
</div>
