<?php
	function build_card_layout(){
		global $i;
		$args = [];
		$cards = [];
		$args['isHeader'] = mason_get_sub_field('card_block_isHeader');
		$rows_arr = mason_get_sub_field('card_block_rows');
		if( is_array($rows_arr) ){
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$image = $row_arr['card_block_image'];
				$card_args = array(
					'subtitle' => $row_arr['card_block_subtitle'],
					'title' => $row_arr['card_block_title'],
					'button' => $row_arr['card_block_link'],
					'id' => 'card_block'.'_'.$i,
					'content' => $row_arr['card_block_content'],
				);
				$card_args['bg_image_url'] = "";
				if( is_array( $image ) ){
					$card_args['bg_image_url'] = $image['url'];
				}
				$cards[] = $card_args;
			}
		}
		$args['cards'] = $cards;
		if( is_array( $args ) ){
			return $args;
		}
	}
	function card_admin_enqueue($hook) {
		wp_register_style( 'card_wp_admin_css', get_template_directory_uri() . '/mason-modules/card/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'card_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'card_admin_enqueue' );
?>