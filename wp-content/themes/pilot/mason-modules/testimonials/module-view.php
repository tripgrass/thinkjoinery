<?php
	global $args;
	global $wp;
	$block_id = $args['acf_incr'];
?>
	<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='slides-section'>
		<div class="feature-wrapper">
	  		<div class="caption-wrapper">
	  			<h6>Client Testimonials</h6>
	  			<?php if(count($args['rows'])>1 ): ?>
					<div class="nav-wrapper" id="test-nav-wrapper-<?php echo $block_id; ?>">
					</div>
				<?php endif; ?>
		  	</div><!--/caption-wrpper-->

			<div class='slick-slider' id="test-slick-slider-<?php echo $block_id; ?>">
				<?php foreach( $args['rows'] as $row ) : ?>
					<div class="testimonial">
						<h4 class="testimonial-content"><?php echo $row['testimonial']; ?></h4>
						<h5 class="testimonial-attribution">-<?php echo $row['attribution']; ?></h5>
					</div>
				<?php endforeach; ?>
	  		</div>
		</div><!--/feature-wrapper-->
	</div><!--/slides-section-->
	<script>
		$(document).ready(function(){

		  $('#test-slick-slider-<?php echo $block_id; ?>').slick({
			adaptiveHeight:true,
			slidesToShow:1,
			appendArrows:$('#test-nav-wrapper-<?php echo $block_id; ?>')
		  });
});
	</script>
<?php endif; ?>