<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => '569d9ff728231',
		'name' => 'footer_form_block',
		'label' => 'Footer Form',
		'display' => 'block',
		'sub_fields' => array (
	        array(
	            'key' => create_key('footer_form','image'),
	            'label' => 'Image',
	            'name' => 'footer_form_block_image',
	            'type' => 'image',
	            'instructions' => '',
	            'required' => 0,
	            'conditional_logic' => array(),
	            'wrapper' => array(
	                'width' => '100%',
	                'class' => '',
	                'id' => '',
	            ),
	            'return_format' => 'array',
	            'preview_size' => 'thumbnail',
	            'library' => 'all',
	            'min_width' => '',
	            'min_height' => '',
	            'min_size' => '',
	            'max_width' => '',
	            'max_height' => '',
	            'max_size' => '',
	            'mime_types' => '',
	        ),
			array (
	            'key' => create_key('footer_form','content'),
				'label' => 'Content',
				'name' => 'footer_form_block_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
		),
		'min' => '',
		'max' => '',
	);
?>