<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="interior-container slower fadeIn animated" >
	<div class="ff-wrap ">
		<div class="ff-content">
			<div class="black" id="footer-img-wrap">
				<img id="footer-img" src="<?php echo $args['image']['url']; ?>">
			</div>
			<div class="white" id="footer-content">	
				<div class="inside-container">
					<?php echo $args['content']; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$( document ).ready(function() {
		function setImgHeight(){
			var $img = $('#footer-img'),
			width = $(window).width(),
			height = $img.height(),			
			correction = 130;
			if( width <= 620 ){
				correction = 0;
			}
			if( width > 620 && width < 960 ){
				correction = 100;
			}
			$('#footer-content').css('margin-top','-'+ correction + 'px').css('padding-top', correction + 'px');
		}
		setImgHeight();
		window.onresize = function() {
			setImgHeight();
		};
	    //alert( "ready!" );
	});
</script>