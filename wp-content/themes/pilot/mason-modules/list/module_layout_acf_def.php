<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key('list','block'),
		'name' => 'list_block',
		'label' => 'List',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key('list','title'),
				'label' => 'Title',
				'name' => 'list_block_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array(
		        'key' => create_key('list','items'),
				'label' => 'List Items',
				'name' => 'list_block_items',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array(
					array(
				        'key' => create_key('list','item'),
						'label' => 'Item',
						'name' => 'item',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),

			),
		),
		'min' => '',
		'max' => '',
	);
?>