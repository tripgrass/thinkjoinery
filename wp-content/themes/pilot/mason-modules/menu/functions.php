<?php
	function build_menu_layout(){
		$args = array(
			'title' => mason_get_sub_field('menu_block_title'),
			'bg' => mason_get_sub_field('menu_block_image'),
		);
		return $args;
	}
?>