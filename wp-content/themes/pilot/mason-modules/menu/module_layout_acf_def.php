<?php
	global $pilot;
	$module = "joinery";
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key($module),
		'name' => 'menu_block',
		'label' => 'Joinery Logo Block',
		'display' => 'block',
		'sub_fields' => array (
	        array(
	            'key' => create_key('menu','image'),
	            'label' => 'Background Image',
	            'name' => 'menu_block_image',
	            'type' => 'image',
	            'instructions' => '',
	            'required' => 0,
	            'conditional_logic' => array(
	            ),
	            'wrapper' => array(
	                'width' => '40%',
	                'class' => '',
	                'id' => '',
	            ),
	            'return_format' => 'array',
	            'preview_size' => 'thumbnail',
	            'library' => 'all',
	            'min_width' => '',
	            'min_height' => '',
	            'min_size' => '',
	            'max_width' => '',
	            'max_height' => '',
	            'max_size' => '',
	            'mime_types' => '',
	        ),

		),
		'min' => '',
		'max' => '',
	);
?>