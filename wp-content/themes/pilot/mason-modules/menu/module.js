jQuery(document).ready(function($) {
	function animateJoinery(){
		if( $('body').hasClass('home')){
			var offset = 4300;
			$timeouts = [100, 200, 250, 300, 400 , 450, 500, 550,, 600, 700,800, 900,1000, 1100,1300,1400,1500,1600];
			$jtimeouts = [450,500,550,700,750,900,1050, 1100,1150];
			$fadeOut = 300;
			$incr = 200;
			$fadeTo5 = 500;
			$fadeTo8 = 800;
			$fadeTo9 = 900;
$offset1 = $offset2 = $offset3 = 0;
$offset2 = 0;
				$mobile = 0;
				if ($(window).width() < 660) {
					$mobile = 1;
				}
				if ($mobile) {
					$("#masthead").addClass('sticky');

					$("body").addClass('stuck');
					$("#waypoint-one").addClass('sticky');
					$("#waypoint-target").addClass('sticky');
				}else{
					var waypoints = $('.way-one-container').waypoint({
						handler: function(direction) {
						//	alert(1);

							$("#waypoint-one").toggleClass('sticky', direction=='down');
							$("#waypoint-target").toggleClass('sticky', direction=='down');
						}
					})

					var waypointtwoi = $('.way-two-container').waypoint({
						handler: function(direction) {
							$("#waypoint-one").toggleClass('trans', direction=='down');
							$("#hero-tag").toggleClass('fade-hero', direction=='down');
						},
						offset:$offset1
					})
					var waypointtwoii = $('.way-two-container').waypoint({
						handler: function(direction) {
							$("#masthead").toggleClass('sticky', direction=='down');

							$("body").toggleClass('stuck', direction=='down');
						},
						offset:$offset2
					})
					var waypoints3 = $('.bottom-section').waypoint({
						handler: function(direction) {
							$("body").toggleClass('stuckBottom', direction=='down');
						},
						offset:$offset3

					})
				}
		}
		else{
		var offset = 0;
			$timeouts = [0, 0,0];
			$jtimeouts = [0];
			$fadeOut = 0;
			$incr = 0;
			$fadeTo5 = 0;
			$fadeTo8 = 0;
			$fadeTo9 = 0;

			$("#masthead").toggleClass('sticky');
			$("body").toggleClass('stuck');
			$("#waypoint-one").toggleClass('sticky');
			$("#waypoint-target").toggleClass('sticky');
			$("body").toggleClass('stuckBottom');
		}
		$('.wordmark-wrapper').click(function(){
			window.location.href = "/";
		})

		if( $('body').hasClass('home')){
			$('.letter:not(.holder)').each( function(){
				$incr = $incr - 7;
				var $this = this;
				var $rand = $timeouts[Math.floor(Math.random() * $timeouts.length)];
				setTimeout(function(){
					$($this).fadeTo( $incr , 0);
				}, $rand);
			})

			setTimeout(function(){
				$('#l1').fadeTo( $fadeTo8 , 0, function() {
				});
			}, 0);
			setTimeout(function(){
				$('.jlogo-wrapper').addClass('active');
				$('#w1').fadeTo( $fadeTo9 , 1,function(){
				});
			}, randT());
			setTimeout(function(){
				$('#l10').fadeTo( $fadeTo8 , 0, function() {
				});
			}, 0);
			setTimeout(function(){
				$('#w10').fadeTo( $fadeTo9 , 1, function(){
				});
			}, randT());
			setTimeout(function(){
				$('#l2').fadeTo( $fadeTo8 , 0, function() {
				});
			}, 0);

			
			setTimeout(function(){
				$('.jlogo-wrapper').addClass('active');
				$('#w2').fadeTo( $fadeTo5 , 1);
			}, randT());
			setTimeout(function(){
				$('#l3').fadeTo( $fadeOut , 0, function() {
					$('#w3').fadeTo( $fadeOut , 1);
		//			$('#hero-tag').fadeTo( $fadeOut + 300 , 1);
					setTimeout(function(){
							//$('#hero-tag').fadeTo( $fadeOut + 300 , 1);
					},  $fadeTo8);
				});
			},  randT());
			setTimeout(function(){
				$('#l6').fadeTo( $fadeOut , 0, function() {
					$('#w6').fadeTo( $fadeOut , 1);
				});
			}, randT() );
			setTimeout(function(){
				$('#l7').fadeTo( $fadeOut , 0, function() {
					$('#w7').fadeTo( $fadeOut , 1);
				});
			}, randT());
			setTimeout(function(){
				$('#l8').fadeTo( $fadeOut , 0, function() {
					$('#w8').fadeTo( $fadeOut , 1);
				});
			}, randT());
			setTimeout(function(){
				$('#l8').fadeTo( $fadeOut , 0, function() {
					$('#w8').fadeTo( $fadeOut , 1);
				});
			}, randT() );
			setTimeout(function(){
				$('#l9').fadeTo( $fadeOut , 0, function() {
					$('#w9').fadeTo( $fadeOut , 1);
				});
			}, randT() );
		}
		else{
			//$('.letter').hide();
			//$('.wordmark-wrapper').find('.letter').fadeTo(0,0);
			//$('.wordmark-wrapper').find('.wordmark').fadeTo(0,1);
		}
	}
	function randT(){
		var rand = Math.floor(Math.random() * $jtimeouts.length),
		val = $jtimeouts[rand];
		$jtimeouts.splice(rand, 1);
		if( $(window).width() > 660 && $('body').hasClass('home')){
			var randIncr = 1500;
		}
		else{
			var randIncr = 0;
		}
		return val + randIncr;
	}
	function rand(){
		return $timeouts[Math.floor(Math.random() * $timeouts.length)];
	}
	if( $('.block-joinery').length > 0  ){
		animateJoinery();
	}
});
