<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 * array	$args['rows']			 //  array of rows of lede/content
	 * string	$args['rows'][0]['lede'] //  row title
	 * string	$args['rows'][0]['hidden_content'] //  row hidden content
	 */
	global $args; 
?>
<?php if( (is_array($args['rows']) && count($args['rows']) > 0 )) : ?>
<div class="container container-md container-sm">
	<div class="row">
		<div class="col-lg-12">
			<?php echo $args['content']; ?>
		</div>
	</div>
	<div class="row">
		<div class="accordion-wrapper col-lg-12">
			<?php $i = 0; if( count($args['rows']) > 0 ) : ?>
				<dl class="accordion">
					<?php if( is_array( $args['rows'] ) ) : ?>
						<?php foreach( $args['rows'] as $row ): ?>
							<dt>
								<div class="row">
									<div class="col-xs-8">
										<h6><b><?php echo $row['lede']; ?></b></h6>
									</div>
									<div class="col-xs-4">
										<?php if ($row['hidden_content']) : ?>
											<div class="acc-tog">
												<div class="horizontal"></div>
												<div class="vertical"></div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</dt>
							<dd>
								<div class="row">
									<div class="col-xs-12 content">
										<?php echo $row['hidden_content']; ?>
									</div>
								</div>
							</dd >
							<?php if( $i == (count($args['rows']) - 1  )) : ?>
								<div class="last-child"></div>
							<?php endif; ?>
						<?php $i++; endforeach; ?>
					<?php endif; ?>
				</dl>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>