<?php
	function build_banner_layout(){
		global $i;

		$banner_args = array(
			'negative_margin' => mason_get_sub_field('banner_block_negative_margin'),
			'negative_margin_amt' => mason_get_sub_field('banner_negative_margin_amt'),
			'subtitle' => mason_get_sub_field('banner_block_subtitle'),
			'button' => mason_get_sub_field('banner_block_link'),
			'id' => 'banner_block_'.$i,
			'content' => mason_get_sub_field('banner_block_content'),
			'title' => mason_get_sub_field('banner_block_title')
		);
		$image = mason_get_sub_field('banner_block_image');
		if( is_array( $image ) ){
			$banner_args['image_url'] = $image['url'];
		}
		if( $left_img = mason_get_sub_field('banner_block_left_image') ){
			$banner_args['left_image'] = $left_img['url'];
		}
		$args = $banner_args;
		if( is_array( $args ) ){
			return $args;
		}
	}
?>