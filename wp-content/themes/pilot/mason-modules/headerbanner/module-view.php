<?php
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
	$block_id = $args['acf_incr'];
?>
<style>
	.slick-list{
		height:<?php echo $args['height']; ?>vh;
	}
</style> 
<?php if( isset($args['parent_title'])):?>
	<div class="parent-header">
		<div class="container content-container interior-container" >
			<div class="title">
				<h6><a href="<?php echo $args['parent_permalink']; ?>"><?php echo $args['parent_title']; ?></a> > <a class="active" href="<?php echo $args['permalink']; ?>"><?php echo $args['title']; ?></a></h6>
				<h1><?php echo $args['title']; ?></h1>
			</div><!--/title-->
		</div><!--/container--> 			
	</div>
	<script>
		$( document ).ready(function() {
			$('body').addClass('has-parent-header');
		});
	</script>
<?php else: ?>
	<?php if(is_array($args['rows'])): $row_counter = 0; ?>
		<?php if($args['isHeader']) : ?>
			<script>
				$( document ).ready(function() {
					$('body').addClass('dark-bg');
				});
			</script>
		<?php endif; ?>
	<div class='slides-section'>
		<div class="feature-wrapper">
			<div class='slick-slider' id="slick-slider-<?php echo $block_id; ?>">
				<?php foreach( $args['rows'] as $row ) : ?>
					<img class='slides-block-image' src='<?php echo $row['image']['url']; ?>'>
				<?php endforeach; ?>
	  		</div>
	  		<div class="caption-wrapper">
	  			<?php if(count($args['rows'])>1 ): ?>
					<div class="nav-wrapper" id="nav-wrapper-<?php echo $block_id; ?>">
					</div>
				<?php endif; ?>
				<div class="slick-caption" id="slick-caption-<?php echo $block_id; ?>">
					<?php foreach( $args['rows'] as $row ) : ?>
						<?php if($row['title'] || $row['subtitle']) : ?>
							<div class="caption" >
								<?php if(is_array($row['link']) && array_key_exists('url',$row['link'])) : ?>
									<a href='<?php echo $row['link']['url']; ?>'><h3><?php echo $row['title']; ?></h3></a>
								<?php else: ?>
									<h3><?php echo $row['title']; ?></h3>
								<?php endif; ?>									
								<h6><?php echo $row['subtitle']; ?></h6>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
		  	</div><!--/caption-wrpper-->
		</div><!--/feature-wrapper-->
	</div><!--/slides-section-->
	<script>
		$(document).ready(function(){
	if( $('#slick-caption-<?php echo $block_id; ?> .caption').length > 0 ){

		  $('#slick-slider-<?php echo $block_id; ?>').slick({
			slidesToShow:1,
			dots:false,
			asNavFor: '#slick-caption-<?php echo $block_id; ?>',
			arrows:false
		  });

		  $('#slick-caption-<?php echo $block_id; ?>').slick({
			slidesToShow:1,
			asNavFor: '#slick-slider-<?php echo $block_id; ?>',
			appendArrows:$('#nav-wrapper-<?php echo $block_id; ?>')
		  });
	}
	else{
	  $('#slick-slider-<?php echo $block_id; ?>').slick({
		slidesToShow:1,
		dots:false,
		arrows:true,
		appendArrows:$('#nav-wrapper-<?php echo $block_id; ?>')
	  });

	}

});
	</script>
<?php endif; ?>
<?php if( 1!=1) : ?>
	<div class="img-block-wrap" id="<?php echo $args['id']; ?>">
		<div class="bg-image" style="background-image: url(<?php echo $args['image_url']; ?>);">
			<div style="width:100%; display:flex; position:absolute; bottom:0;">
				<div class="container content-container interior-container dark-card">
					<div class="title ">
						<?php if( isset( $args['title'] ) ) : ?>
							<div class=" media_title">
								<?php echo $args['title']; ?>
							</div><!--/text_effect-->
						<?php endif; ?>
					</div><!--/title-->
	        	</div><!--/container--> 			
	        </div>
		</div><!--/bg-img-->
	</div><!--img-block-wrap-->
	<?php endif; ?>

<?php endif; ?>