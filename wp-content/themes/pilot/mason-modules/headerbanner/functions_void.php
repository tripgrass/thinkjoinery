<?php
	function build_headerbanner_layout(){
		global $post;
		if( 1==1 || get_field('use_default_header') ){
			$args = array(
				'title' => get_the_title()
			);
	//		$image = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
	        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large');
	        if($thumb_url_array[0]){
		        $args['image_url'] = $thumb_url_array[0];
			}
			else{
				$image = mason_get_sub_field('headerbanner_block_image');
				if( is_array( $image ) ){
					$args['image_url'] = $image['url'];
				}
			}
			if( is_array( $args ) ){
				return $args;
			}
		}
		elseif( $parent = wp_get_post_parent_id( $post->ID ) ){
			$args = array(
				'id' => 'headerbanner_block_'.$i,
				'title' => get_the_title(),
				'permalink' => get_permalink(),
				'parent_title' => get_the_title($parent),
				'parent_permalink' => get_permalink($parent)
			);
			return $args;
		}
	}
?>