<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_headerbanner_layout(){
		$rows_arr = mason_get_sub_field('headerbanner_block_rows');
		$args = [];
		$args['isHeader'] = mason_get_sub_field('headerbanner_block_isHeader');
		$height = mason_get_sub_field('headerbanner_block_height');
		$args['height'] = ($height ? $height : 99);
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$rows[] = array(
					'title' => $row_arr['headerbanner_block_title'],
					'link' => $row_arr['headerbanner_block_link'],
					'subtitle' => $row_arr['headerbanner_block_subtitle'],
					'image' => $row_arr['headerbanner_block_image'],
				);
				
			}
			while ( mason_have_rows('headerbanner_block_rows') ) : the_row();
				$rows[] = array(
					'title' => get_sub_field('headerbanner_block_title'),
					'link' => $row_arr['headerbanner_block_link'],
					'subtitle' => get_sub_field('headerbanner_block_subtitle'),
					'image' => get_sub_field('headerbanner_block_image'),
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		return $args;
	}
function wp_durazo_enqueue_scripts() {
	wp_enqueue_script( 'slide_script', get_template_directory_uri() . "/mason-modules/headerbanner/script.js", array('jquery'), null, true );
	wp_enqueue_script( 'slick_script', get_template_directory_uri() . "/mason-modules/headerbanner/slick.min.js" , array('jquery'), null, true);
	wp_enqueue_style( 'slick_style',"https://kenwheeler.github.io/slick/slick/slick-theme.css");
	wp_enqueue_style( 'slick_theme_style',"https://kenwheeler.github.io/slick/slick/slick.css");

	wp_enqueue_script( 'popup_script', get_template_directory_uri() . "/mason-modules/headerbanner/html5lightbox.js" , array('jquery'), null, true);
//	wp_enqueue_style( 'popup_style',get_template_directory_uri() . "/mason-modules/slides/slick-lightbox.css");

}
add_action( 'wp_enqueue_scripts', 'wp_durazo_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'wp_durazo_enqueue_scripts' );

?>