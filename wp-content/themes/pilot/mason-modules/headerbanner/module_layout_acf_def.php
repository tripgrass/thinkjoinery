<?php

	global $pilot;
	// add module layout to flexible content 
	$name = "headerbanner";
	$module_layout = array (
		'key' => create_key($name, 'block'),
		'name' => 'headerbanner_block',
		'label' => 'Slideshow / Header',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key($name,'isHeader'),
				'label' => 'Use as Header',
				'name' => 'headerbanner_block_isHeader',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),	
			array (
				'key' => create_key($name,'height'),
				'label' => 'Height (%)',
				'name' => 'headerbanner_block_height',
				'type' => 'text',
				'instructions' => 'Set as 1 to 100',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),

			array (
				'key' => create_key($name,'rows'),
				'label' => 'Images',
				'name' => 'headerbanner_block_rows',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'block',
				'button_label' => 'Add Row',
				'sub_fields' => array (

					array (
						'key' => create_key($name,'title'),
						'label' => 'Title',
						'name' => 'headerbanner_block_title',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => create_key($name,'link'),
						'label' => 'Link',
						'name' => 'headerbanner_block_link',
						'type' => 'link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => create_key($name,'subtitle'),
						'label' => 'Subtitle',
						'name' => 'headerbanner_block_subtitle',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => create_key($name,'image'),
						'label' => 'Image',
						'name' => 'headerbanner_block_image',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
			),
		),
		'min' => '',
		'max' => '',
	);
?>