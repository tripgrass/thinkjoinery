<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_alternating_layout(){
		$args['start'] = mason_get_sub_field('alternating_block_start');

		if( mason_have_rows('alternating_block_rows') ):
			$rows = array();
			while ( mason_have_rows('alternating_block_rows') ) : the_row();
				$rows[] = array(
					'title' => get_sub_field('alternating_block_title'),
					'text' => get_sub_field('alternating_block_text'),
					'image' => get_sub_field('alternating_block_image'),
					'image_class' => get_sub_field('alternating_block_image_class'),
					'button' => get_sub_field('alternating_block_button'),
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		return $args;
	}

?>