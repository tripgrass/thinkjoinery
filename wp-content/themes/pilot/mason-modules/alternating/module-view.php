<?php
	/**
	 * string	$args['rows']
	 * string	$args['rows'][0]['image']
	 * string	$args['rows'][0]['title']
	 * string	$args['rows'][0]['text']
	 * string	$args['rows'][0]['button_text']
	 * string	$args['rows'][0]['button_link']
	 */
	global $args;
?>
<?php if(is_array($args['rows'])): 
	$row_counter = 1; if($args['start']){ $row_counter = 0; }
 ?>
		<div class="header-wrap">
	</div>

	<?php foreach( $args['rows'] as $row ) : ?>
			<?php 
				$image_content = "<div class='alt-block-image ".$row['image_class']."' style='background-image: url(".$row['image']['url'].")'></div>";
				$text_content = "
					<div class='alt-content-section wow '>
						<div class=''>
							<h2>".$row['title']."</h2>
							<div class='line'></div>
							<div class='text'>".$row['text']."</div>";
							if( $row['button']  ){
								$text_content .= "<div class='button-wrapper '>
										<a href='". $row['button']['url'] ."' target='". $row['button']['target'] ."' class='pilot-button'>
											<h5>". $row['button']['title'] ."</h5>
										</a>
									</div>";
							}
							$text_content .= "
						</div><!--innersection--->
					</div><!--alt-content-section--->";
			?>
			<?php $class = ""; if( $row_counter%2 ){ $class = "image-right"; } $row_counter++;?>
			<div class="alternating-section  <?php echo $class; ?>">
					<?php echo $image_content.$text_content; ?>	
		  </div><!--/alternating-section--->
			<div class="clear"></div>  

	<?php endforeach; ?>

<?php endif; ?>