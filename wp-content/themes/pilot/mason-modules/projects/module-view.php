<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$card_class = "";
?>
<div class="<?php echo $card_class; ?> " >
	<div class="projects-wrap ">
		<div class="projects-content">
			<div class="labels">
				<h3><?php echo $args['title']; ?></h3>
				<a href='/projects'><h5>VIEW ALL</h5></a>
			</div>
			<div class="projects">
				<?php if(count($args['projects'])>0) : ?>
					<?php foreach($args['projects'] as $project) : 
						$obj = new PilotProject($project); ?>
						<a href="<?php echo get_permalink($obj->ID); ?>"><div class="mix" style="background-image:url('<?php echo $obj->thumbnail; ?>')">
                      <div class="mix-caption"><h6><?php echo $obj->post_title; ?></h6></div>
                    </div></a>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>