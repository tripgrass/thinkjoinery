<?php
	function build_projects_layout(){
		$source = mason_get_sub_field('projects_block_source');
		if($source){
			$projects = mason_get_sub_field('projects_block_projects');
		}
		else{
			$number = mason_get_sub_field('projects_block_random_number');
			if(!$number){
				$number = 6;
			}
			$projects = get_posts([
			  'post_type' => 'project',
			  'post_status' => 'publish',
			  'numberposts' => $number
			]);
		}
		$args = array(
			'title' => (mason_get_sub_field('projects_block_title') ? mason_get_sub_field('projects_block_title') : 'Projects'),
			'projects' => $projects
		);
		return $args;
	}
?>