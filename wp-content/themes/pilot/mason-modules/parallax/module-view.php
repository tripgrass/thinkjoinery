<?php
	global $args;
	$block_id = $args['acf_incr'];
?>
<div class="parallax-block-wrap">
	<div class="bg-image" style="background-image: url(<?php echo $args['image']['url']; ?>);">
		<div style="width:100%; display:flex; position:absolute; bottom:0;">
			<div class="container content-container interior-container dark-card">
				<div class="title ">
					<?php if( isset( $args['title'] ) ) : ?>
						<div class=" media_title">
							<?php echo $args['title']; ?>
						</div><!--/text_effect-->
					<?php endif; ?>
				</div><!--/title-->
        	</div><!--/container--> 			
        </div>
	</div><!--/bg-img-->
</div><!--img-block-wrap-->