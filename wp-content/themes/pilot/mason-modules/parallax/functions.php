<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_parallax_layout(){
		$args = array(
			'title' => mason_get_sub_field('parallax_block_title'),
			'image' => mason_get_sub_field('parallax_block_image')
		);
		return $args;
	}
?>