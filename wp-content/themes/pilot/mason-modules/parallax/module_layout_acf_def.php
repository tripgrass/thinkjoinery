<?php

	global $pilot;
	// add module layout to flexible content 
	$name = "parallax";
	$module_layout = array (
		'key' => create_key($name, 'block'),
		'name' => 'parallax_block',
		'label' => 'Parallax',
		'display' => 'block',
		'sub_fields' => array (

			array (
				'key' => create_key($name,'title'),
				'label' => 'Title',
				'name' => 'parallax_block_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => create_key($name,'image'),
				'label' => 'Image',
				'name' => 'parallax_block_image',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
		),
		'min' => '',
		'max' => '',
	);
?>