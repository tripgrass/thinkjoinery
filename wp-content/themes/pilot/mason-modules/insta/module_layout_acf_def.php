<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key('insta','block'),
		'name' => 'insta_block',
		'label' => 'Instagram Feed',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key('insta','content'),
				'label' => 'Content',
				'name' => 'insta_block_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
		),
		'min' => '',
		'max' => '',
	);
?>