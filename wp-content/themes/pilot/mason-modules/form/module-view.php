<?php 
	global $args; 
?>
	<div class=" container container-lg container-md container-sm ">
		<div class="row">
			<div class="col-xs-12">
				<div class="form-container" >
					<?php echo do_shortcode("[gravityform id='" . $args['form'] . "' description='false']"); ?>
				</div>
			</div>
		</div>
	</div>
