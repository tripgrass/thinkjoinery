<?php
global $pilot;
// add module layout to flexible content
    $myforms = RGFormsModel::get_forms();
//    print_r($myforms);
    $forms = [];
    foreach($myforms as $iForm){
        //print_r($iForm);
        $forms[$iForm->id] = $iForm->title;
    }

    // add module layout to flexible content 
    $name = "form";
    $module_layout = array (
        'key' => create_key($name, 'block'),
        'name' => 'form_block',
        'label' => 'Form',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => create_key($name,'form'),
                'label' => 'Select a Form',
                'name' => 'form_block_form',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => $forms,
                'default_value' => array(
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'return_format' => 'value',
                'ajax' => 0,
                'placeholder' => '',            
            ),
        ),);

?>