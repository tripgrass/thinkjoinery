<?php
	/**
	 * string	$args['rows']
	 * string	$args['rows'][0]['image']
	 * string	$args['rows'][0]['title']
	 * string	$args['rows'][0]['text']
	 * string	$args['rows'][0]['button_text']
	 * string	$args['rows'][0]['button_link']
	 */
	global $args;
?>
<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='team-section'>
		<div class='team-content-section interior-box-wide'>
			<script>
				var team = {};
			</script>
	<?php $i = 1; foreach( $args['rows'] as $row ) : ?>
					<script>
						var member<?php echo $i; ?> = {};
						member<?php echo $i; ?>.name = '<?php echo $row['name']; ?>';
						member<?php echo $i; ?>.text = '<?php echo preg_replace("#'#","\'",$row['text']); ?>';
						team[<?php echo $i; ?>] = member<?php echo $i; ?>;
					</script>
					<div class='member shadow-card' data-member='<?php echo $i; ?>'>
							<div class='team-block-image'>
								<img src='<?php echo $row['image']['url']; ?>'>
							</div>
							<div class="member-content">
								<h5 class="member-title"><?php echo $row['name']; ?></h5>
								<h6><?php echo $row['title']; ?></h6>
							</div>
					</div><!--team-content-section--->
	<?php $i++; endforeach; ?>
  		</div><!--/team-section--->
	</div>
	<script>
		console.log('team:',team);
		 $(document).ready(function() {
		 	var divs = $(".member"),
		 	width = $(window).width();
		 	$incr = 3;
		 	if(width < 800){
		 		$incr = 1;
		 	}
		 	for(var i = 0; i < divs.length; i+=$incr) {
  divs.slice(i, i+$incr).wrapAll("<div class='member-row'></div>");
}
$('.member-row ').append('<div class="popup-content shadow-card"><div class="blue-line"></div><div class="popup-wrapper"><h2 class="name"></h2><p class="text"></p></div></div>');
		 	$('.member.shadow-card').on('click',function(){
		 		$('.member.shadow-card').removeClass('active');
		 		var member_id = $(this).data('member'),
		 		member = team[member_id];
		 		$('.popup-content').removeClass('active');
		 		var $popup = $(this).siblings('.popup-content');
		 		$popup.find('.name').html(member.name);
		 		$popup.find('.text').html(member.text);
		 		$popup.addClass('active');
		 	});
		 });
	</script>
<?php endif; ?>