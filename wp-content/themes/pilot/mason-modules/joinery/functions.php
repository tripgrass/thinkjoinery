<?php
	function build_joinery_layout(){
		$args = array(
			'title' => mason_get_sub_field('joinery_block_title'),
			'bg' => mason_get_sub_field('joinery_block_image'),
		);
		return $args;
	}
?>