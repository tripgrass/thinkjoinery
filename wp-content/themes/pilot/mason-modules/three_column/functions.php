<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}

	function build_three_column_layout(){
		$args = array(
			'title' => mason_get_sub_field('three_column_block_title'),
			'columns' => mason_get_sub_field('three_column_block_columns'),
		);
		return $args;
	}
?>