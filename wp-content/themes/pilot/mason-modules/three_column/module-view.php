<?php 
	/**
	 * string	$args['title']
	 * string	$args['columns']
	 */
	global $args;
?>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700" rel="stylesheet">
<div class="three-col-wrap">
	<div class="">
		<?php $i = 1; foreach( $args['columns'] as $col ) : ?>
			<?php if($col['title']) : ?>
				<div class="column-wrap">
					<div id="col-<?php echo $i; ?>" style="background-image: url(<?php echo $col['icon']['url']; ?>);">
					</div>
						<h3><?php echo $col['title']; ?></h3>
						<?php if($col['text']) : ?>
							<div class="three-col-content"><?php echo $col['text']; ?></div>
						<?php endif; ?>
				</div>
			<?php endif; ?>
		<?php $i++; endforeach; ?>
	</div>
</div>
