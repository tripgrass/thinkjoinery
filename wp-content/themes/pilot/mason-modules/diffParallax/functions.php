<?php
	function build_diffParallax_layout(){
		$args = array(
			'image_one' => mason_get_sub_field('diffParallax_block_image_one'),
			'image_two' => mason_get_sub_field('diffParallax_block_image_two'),
		);
		return $args;
	}
?>