<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key('generic_content','block'),
		'name' => 'generic_content_block',
		'label' => 'Generic Content Theme',
		'display' => 'block',
		'sub_fields' => array (
			array (
		        'key' => create_key('generic_content','isHeader'),
				'label' => 'Use as Header',
				'name' => 'generic_content_block_isHeader',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
		        'key' => create_key('generic_content','title'),
				'label' => 'Title',
				'name' => 'generic_content_block_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
		        'key' => create_key('generic_content','content'),
				'label' => 'Content',
				'name' => 'generic_content_block_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
		),
		'min' => '',
		'max' => '',
	);
?>