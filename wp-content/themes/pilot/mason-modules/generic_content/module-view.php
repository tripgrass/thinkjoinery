<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$card_class = "";
	if( $args['use_card'] ){
		$card_class = "shadow-card";
	}
	$isHeader = "";
	if($args['isHeader']) : 
		$isHeader = " isHeader";
	?>
	<script>
		$( document ).ready(function() {
			$('body').addClass('gc-header');
		});
	</script>
<?php endif; ?>
<div class="<?php echo $card_class; ?> interior-container interior-box <?php echo $isHeader; ?>" >
	<div class="gc-wrap ">
		<div class="gc-content">
			<?php if($args['title']) : ?>
				<h1><?php echo $args['title']; ?></h1>
			<?php endif; ?>
			<?php echo $args['content']; ?>
		</div>
	</div>
</div>