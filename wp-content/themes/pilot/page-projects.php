<?php
/*
Template Name: Projects
*/
$services = get_posts([
  'post_type' => 'service',
  'post_status' => 'publish',
  'numberposts' => -1
]);
$projects = get_posts([
  'post_type' => 'project',
  'post_status' => 'publish',
  'numberposts' => -1
]);
 get_header(); ?>
    <div class="buffer"></div>

    <?php if ( have_posts() ) : ?>
            <div class="interior-box control-wrapper closed">        
              <div class="mobile-selector">
                <span class="mobile-selected"><h6>ALL</h6></span>
                <div class="arrows">
                    <i class="arrow up"></i><i class="arrow down"></i>
                </div>
              </div>
        		  <div class="controls ">
                  <span data-filter="all"><h6>All</h6></span>
                    <?php foreach($services as $service):

                     ?>
                        <span data-filter=".<?php echo $service->post_name; ?>"><h6><?php echo $service->post_title; ?></h6></span>
                    <?php endforeach; ?>
                </div>
                <div class="mobile-bottom"></div>
            </div>
            <div class="container" id="MixItUpDC6F17" style="">
                <?php foreach($projects as $project) :
                    $obj = new PilotProject($project);
//                    print_r($obj);
                    $classes = "";
                    foreach($obj->services as $service){
                        $classes .= " ".$service['slug'];
                    }

                 ?>
                    <a href="<?php echo get_permalink($obj->ID); ?>"><div class="mix <?php echo $classes; ?>" style="background-image:url('<?php echo $obj->thumbnail; ?>')">
                      <div class="mix-caption"><h6><?php echo $obj->post_title; ?></h6></div>
                    </div></a>
                <?php endforeach; ?>
                <div class="gap"></div>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        <script>
        	$(document).ready(function(){
	            var containerEl = document.querySelector('.container');
	            var mixer = mixitup(containerEl);
	        });
        </script>
        <script>
          jQuery(document).ready(function($) {
            console.log($('.arrows'));
            function checkSelect(){
              var open = $('.control-wrapper').hasClass('closed');
              if(open){
                $('.control-wrapper').removeClass('closed');
                $('.up').show();
                $('.down').hide();
                $('.mobile-selected').hide();
              }
              else{
                $('.control-wrapper').addClass('closed');
                $('.down').show();              
                $('.up').hide();              
                $('.mobile-selected').show();
              }

            }
            $('.arrows').on('click',function(){
                checkSelect();
            })
            $('.controls').on('click',function(){
              var active = $('.mixitup-control-active h6');
              $selected = $(active).html();
              $('.mobile-selected h6').html($selected);
                checkSelect();

            })
          });
        </script>
	<?php endif; ?>
<style>
	

/* Controls
---------------------------------------------------------------------- */



</style>
<?php get_footer(); ?>