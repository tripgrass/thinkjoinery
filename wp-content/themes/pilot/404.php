<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

<div class="block-generic_content module    " id="generic_content_block_1">
	<div class="layout-content">
		<div class=" interior-container interior-box ">
			<div class="gc-wrap ">
				<div class="gc-content">
					<br>
					<br>
					<br>
					<br>
					<h1>404</h1>
					<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'pilot' ); ?></p>

					<?php
						//get_search_form();

					?>

				</div>
			</div>
		</div>
	</div><!--/layout-content-->
</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
