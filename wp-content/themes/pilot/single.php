<?php get_header(); ?>
<?php //get_all_blocks(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', pilot_get_view_format() ); ?>
<?php
$locations = get_field('service_projects');

						?>
						<?php if( $locations ): ?>
							<ul>
							<?php foreach( $locations as $location ): ?>
								<li>
									<a href="<?php echo get_permalink( $location->ID ); ?>">
										<?php echo get_the_title( $location->ID ); ?>
									</a>
								</li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>

?>
			<?php pilot_get_comments(); ?>
			<?php the_post_navigation(); ?>
		<?php endwhile; ?>

<?php get_footer(); ?>