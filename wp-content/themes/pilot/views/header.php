<?php 
	echo do_shortcode('[mason_build_blocks container=header]');
//	echo do_shortcode('[mason_build_blocks container=header is_option="1"]'); 
?>
<div class="way-two-container">
	<header id="masthead" class="site-header">
	    <div class="top_section container" id="waypoint-two">
	        <nav id="site-navigation" class="main-navigation">
				<?php echo do_shortcode('[mason_build_blocks container=navigation is_option="1"]'); ?>	            
	            <div id="c-mask" class="c-mask"></div><!-- /c-mask -->
	        </nav>
	    </div>
    	<div class="bottom-spacer"></div>
	</header>
</div>
<div class="bottom-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 tag-text">
				<?php echo get_field('blurb','option'); ?>
	   			<a class='pilot-button white' style="float:right" href='/contact-us'><h5>Get In Touch</h5></a>
	   		</div>
    	</div>
    </div>
</div>
