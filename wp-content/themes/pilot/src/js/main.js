(function() {

	// Configure WOW if using animations

    /*wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();*/

})();

jQuery(document).ready(function($) {
	// Initiate magnific

	$(".mfp-media a").magnificPopup({
		type:'iframe',
		closeMarkup: '<button title="%title%" type="button" class="mfp-close"></button>'
	});

	$(".mfp-slide").magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}
	});
$.fn.isInView = function() {
	var elementTop = $(this).offset().top;
	var elementBottom = elementTop + $(this).outerHeight();
	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();
	return elementBottom > viewportTop && elementTop < viewportBottom;
};	
	var $footer_menu = $(".mobile-navigation").clone().appendTo( $('.footer-content') );
	//console.log($footer_menu);
	$footer_menu.find('.menu-close-wrapper-close').remove();
	$footer_menu.find('.container-fluid.container-lg').removeClass('container-card');
	$('.bottom-section .pilot-button').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({
			scrollTop: ( $(".site-footer #gform_1").offset().top - 90)
		}, 500);
    })
	$('.menu-button,.menu-button-close').click(function(){
		if( $('body').hasClass('menu-scroll') ){
			$('body').removeClass('open-mobile');
			$('body').removeClass('menu-scroll');
			window.scrollTo(0,window.scrollPosition);
		}
		else{
			$('body').addClass('open-mobile');
			$('body').addClass('menu-scroll');
			var scrollPosition = $(document).scrollTop();
			window.scrollPosition = scrollPosition;
			var footerHeight = $('.block-footer').height(),
				bodyHeight = $(document).height(),//document.body.scrollHeight,
				scrollToHeight = parseInt(bodyHeight - footerHeight - 60);
			window.scrollTo(0, scrollToHeight);
			$(window).scroll( function(){
				if( !$('.site-footer').isInView() ){
					$('body').removeClass('open-mobile');
					$('body').removeClass('menu-scroll');				
				}
			})
		}

	});

function setupAccordion(){	
	var allPanels = $('.accordion > dd').hide(),
	allPanelLabels = $('.accordion > dt');
	
	$('.accordion > dt .acc-tog').click(function() {
		if ( !$(this).closest('dt').hasClass('active js-open') ) {
			//allPanels.slideUp();
			//allPanelLabels.removeClass('active js-open');
			$(this).closest('dt').next().slideDown();
			$(this).closest('dt').addClass('active js-open');
		}
		else {
			$(this).closest('dt').removeClass('active js-open').next().slideUp();
		}
		return false;
	});
}

setupAccordion();

});
